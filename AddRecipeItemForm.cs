﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SotaDB_CSV_Edition
{
    public partial class AddRecipeItemForm : Form
    {
        private readonly List<Recipe> recipes;

        public string Item { get; set; }

        public AddRecipeItemForm(List<Recipe> recipes)
        {
            this.recipes = recipes;
            Item = null;

            InitializeComponent();
        }

        private void FillList(string filter = null)
        {
            listBox1.Items.Clear();

            string ucFilter = null;

            if(filter != null)
            {
                ucFilter = filter.ToUpperInvariant();
            }

            foreach(Recipe recipe in recipes)
            {
                if (ucFilter != null)
                {
                    if (!recipe.name.ToUpperInvariant().Contains(ucFilter))
                        continue;
                }

                listBox1.Items.Add(recipe.name);
            }
        }

        private void AddRecipeItemForm_Load(object sender, EventArgs e)
        {
            FillList();

            textBox1.Focus();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            buttonOk.Enabled = listBox1.SelectedIndex != -1;
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            var sb = new StringBuilder();

            sb.Append(listBox1.SelectedItem.ToString());

            if(numericUpDownQuantity.Value > 1)
            {
                sb.Append($" ({numericUpDownQuantity.Value})");
            }

            Item = sb.ToString();

            DialogResult = DialogResult.OK;

            Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            FillList(textBox1.Text);
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
