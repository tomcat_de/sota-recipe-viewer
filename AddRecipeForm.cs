﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SotaDB_CSV_Edition
{
    public partial class AddRecipeForm : Form
    {
        public Recipe recipe { get; set; }

        private readonly List<Recipe> recipes;

        public AddRecipeForm(List<Recipe> recipes)
        {
            this.recipes = recipes;

            InitializeComponent();
        }

        private void AddRecipeForm_Load(object sender, EventArgs e)
        {
            comboBoxSkill.Items.Add(Recipe.Skill.Alchemy);
            comboBoxSkill.Items.Add(Recipe.Skill.Blacksmithing);
            comboBoxSkill.Items.Add(Recipe.Skill.Butchery);
            comboBoxSkill.Items.Add(Recipe.Skill.Carpentry);
            comboBoxSkill.Items.Add(Recipe.Skill.Cooking);
            comboBoxSkill.Items.Add(Recipe.Skill.Gathering);
            comboBoxSkill.Items.Add(Recipe.Skill.Milling);
            comboBoxSkill.Items.Add(Recipe.Skill.Smelting);
            comboBoxSkill.Items.Add(Recipe.Skill.Tailoring);
            comboBoxSkill.Items.Add(Recipe.Skill.Tanning);
            comboBoxSkill.Items.Add(Recipe.Skill.Textiles);
            comboBoxSkill.Items.Add(Recipe.Skill.Tool);


            if (recipe != null)
            {
                textBoxName.Text = recipe.name;

                for (int i = 0; i < comboBoxSkill.Items.Count; ++i)
                {
                    var skill = (Recipe.Skill)comboBoxSkill.Items[i];

                    if (skill == recipe.skill)
                    {
                        comboBoxSkill.SelectedIndex = i;
                        break;
                    }
                }


                if (recipe.input != null)
                {
                    foreach (string s in recipe.input)
                    {
                        listBoxItems.Items.Add(s);
                    }
                }
            }

            buttonUsedIn.Enabled = (recipe != null);
        }

        private void UpdateOkButton()
        {
            button1.Enabled =   textBoxName.Text.Length > 1 &&
                                comboBoxSkill.SelectedItem != null;
        }

        private void textBoxName_TextChanged(object sender, EventArgs e)
        {
            UpdateOkButton();
        }

        private void comboBoxSkill_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateOkButton();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if(recipe == null)
                recipe = new Recipe();

            recipe.name = textBoxName.Text;
            recipe.skill = (Recipe.Skill) comboBoxSkill.SelectedItem;
            recipe.input = new string[listBoxItems.Items.Count];

            for(int i = 0; i < listBoxItems.Items.Count; ++i)
            {
                recipe.input[i] = listBoxItems.Items[i].ToString();
            }

            DialogResult = DialogResult.OK;

            Close();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            buttonRemove.Enabled = listBoxItems.SelectedIndex != -1;
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            using (AddRecipeItemForm addItem = new AddRecipeItemForm(recipes))
            {
                var result = addItem.ShowDialog();

                if(result == DialogResult.OK)
                {
                    listBoxItems.Items.Add(addItem.Item);
                }
            }
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            listBoxItems.Items.RemoveAt(listBoxItems.SelectedIndex);
        }

        private void buttonUsedIn_Click(object sender, EventArgs e)
        {
            if (recipe == null)
                return;

            var sb = new StringBuilder();

            var n = 0;

            // Extract item's name without quantity information
            var nameUpper = recipe.name.ToUpperInvariant().Trim();

            foreach (Recipe r in recipes)
            {
                if (r == recipe  || r.input == null)
                    continue;

                foreach (var item in r.input)
                {
                    var upperCase = item.ToUpperInvariant().Trim();

                    // Extract quantity information
                    var index = upperCase.LastIndexOf('(');

                    if (index > -1)
                    {
                        upperCase = upperCase.Substring(0, index - 1).TrimEnd();
                    }

                    if (upperCase.CompareTo(nameUpper) == 0)
                    {
                        sb.Append(r.name);
                        sb.Append('\n');

                        n += 1;
                    }
                }
            }

            var sPlural = n == 1 ? "" : "s";

            var msg = (n == 0 ? $"{recipe.name} is not used in any recipes!" : $"{recipe.name} is used in {n} recipe{sPlural}:\n\n");

            sb.Insert(0, msg);

            MessageBox.Show(sb.ToString(), "SotA Recipe Viewer", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                Close();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
