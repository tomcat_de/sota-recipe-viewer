﻿using System;
using System.Collections.Generic;
using System.Resources;
using System.Text;
using System.Windows.Forms;

namespace SotaDB_CSV_Edition
{
    public partial class Materials : Form
    {
        const int columnWidth = 200;

        readonly IEnumerable<Recipe> recipes;

        private enum MaterialMeta
        {
            Iron,
            MeteoricIron,
            Copper,
            Bronze,
            WhiteIron,
            Constantan,
            Leather,
            SuppleLeather,
            HardenedLeather,
            Cotton,
            Fustian,
            Carapacian,
            Pine,
            GreyPine,
            CrimsonPine,
            Maple,
            HardMaple,
            RockMaple
        }

        private Dictionary<MaterialMeta, string> MaterialMetaText;

        public Materials(IEnumerable<Recipe> recipes)
        {
            this.recipes = recipes;

            InitializeComponent();

            // Create columns
            int nColumnMaterial         = listView1.Columns.Add(new ColumnHeader() { Text = "Material" });
            int nColumnBladesBludgeon   = listView1.Columns.Add(new ColumnHeader() { Text = "Blade & Bludgeon" });
            int nColumnRanged           = listView1.Columns.Add(new ColumnHeader() { Text = "Ranged" });
            int nColumnStaff            = listView1.Columns.Add(new ColumnHeader() { Text = "Staff" });
            int nColumnWand             = listView1.Columns.Add(new ColumnHeader() { Text = "Wand" });
            int nColumnArmor            = listView1.Columns.Add(new ColumnHeader() { Text = "Armor" });

            foreach(ColumnHeader col in listView1.Columns)
            {
                col.Width = columnWidth;
            }

            // Create groups
            int nGroupMetals            = listView1.Groups.Add(new ListViewGroup() { Header = "Metals" });
            int nGroupLeatherTextiles   = listView1.Groups.Add(new ListViewGroup() { Header = "Leather & Textiles" });
            int nGroupWood              = listView1.Groups.Add(new ListViewGroup() { Header = "Wood" });

            // Create Metals
            ListViewItem item = listView1.Items.Add("Iron");
            item.Group = listView1.Groups[nGroupMetals];
            item.Tag = MaterialMeta.Iron;
            item.SubItems.Add("10% Melee Damage");
            item.SubItems.Add("");
            item.SubItems.Add("");
            item.SubItems.Add("10% Melee Damage");
            item.SubItems.Add("10% Damage Resistance");

            item = listView1.Items.Add("Meteoric Iron");
            item.Group = listView1.Groups[nGroupMetals];
            item.Tag = MaterialMeta.MeteoricIron;
            item.SubItems.Add("20% Melee Damage");
            item.SubItems.Add("");
            item.SubItems.Add("");
            item.SubItems.Add("20% Melee Damage");
            item.SubItems.Add("20% Damage Resistance");

            item = listView1.Items.Add("Copper");
            item.Group = listView1.Groups[nGroupMetals];
            item.Tag = MaterialMeta.Copper;
            item.SubItems.Add("10% Attack Speed");
            item.SubItems.Add("");
            item.SubItems.Add("");
            item.SubItems.Add("10% Attack Speed");
            item.SubItems.Add("10% Damage Avoidance");

            item = listView1.Items.Add("Bronze");
            item.Group = listView1.Groups[nGroupMetals];
            item.Tag = MaterialMeta.Bronze;
            item.SubItems.Add("20% Attack Speed");
            item.SubItems.Add("");
            item.SubItems.Add("");
            item.SubItems.Add("20% Attack Speed");
            item.SubItems.Add("20% Damage Avoidance");

            item = listView1.Items.Add("White Iron");
            item.Group = listView1.Groups[nGroupMetals];
            item.Tag = MaterialMeta.WhiteIron;
            item.SubItems.Add("10% Knockdown Chance");
            item.SubItems.Add("");
            item.SubItems.Add("");
            item.SubItems.Add("10% Knockdown Chance");
            item.SubItems.Add("2 Strength");

            item = listView1.Items.Add("Constantan");
            item.Group = listView1.Groups[nGroupMetals];
            item.Tag = MaterialMeta.Constantan;
            item.SubItems.Add("2 Dexterity");
            item.SubItems.Add("");
            item.SubItems.Add("");
            item.SubItems.Add("2 Dexterity");
            item.SubItems.Add("2 Dexterity");

            // Create woods
            item = listView1.Items.Add("Pine");
            item.Group = listView1.Groups[nGroupWood];
            item.Tag = MaterialMeta.Pine;
            item.SubItems.Add("10% Attack Speed");
            item.SubItems.Add("10% Attack Speed");
            item.SubItems.Add("10% Attack Speed");
            item.SubItems.Add("10% Attack Speed");
            item.SubItems.Add("");

            item = listView1.Items.Add("Grey Pine");
            item.Group = listView1.Groups[nGroupWood];
            item.Tag = MaterialMeta.GreyPine;
            item.SubItems.Add("20% Attack Speed");
            item.SubItems.Add("20% Attack Speed");
            item.SubItems.Add("20% Attack Speed");
            item.SubItems.Add("20% Attack Speed");
            item.SubItems.Add("");

            item = listView1.Items.Add("Crimson Pine");
            item.Group = listView1.Groups[nGroupWood];
            item.Tag = MaterialMeta.CrimsonPine;
            item.SubItems.Add("20% Magic Damage");
            item.SubItems.Add("20% Magic Damage");
            item.SubItems.Add("20% Magic Damage");
            item.SubItems.Add("20% Magic Damage");
            item.SubItems.Add("");

            item = listView1.Items.Add("Maple");
            item.Group = listView1.Groups[nGroupWood];
            item.Tag = MaterialMeta.Maple;
            item.SubItems.Add("10% Damage");
            item.SubItems.Add("10% Damage");
            item.SubItems.Add("10% Damage");
            item.SubItems.Add("10% Damage");
            item.SubItems.Add("");

            item = listView1.Items.Add("Hard Maple");
            item.Group = listView1.Groups[nGroupWood];
            item.Tag = MaterialMeta.HardMaple;
            item.SubItems.Add("20% Damage");
            item.SubItems.Add("20% Damage");
            item.SubItems.Add("20% Damage");
            item.SubItems.Add("10% Damage"); // ???
            item.SubItems.Add("");

            item = listView1.Items.Add("Rock Maple");
            item.Group = listView1.Groups[nGroupWood];
            item.Tag = MaterialMeta.RockMaple;
            item.SubItems.Add("10% Damage");
            item.SubItems.Add("20% Range Increase");
            item.SubItems.Add("10% Damage (?)");
            item.SubItems.Add("20% Range Increase");
            item.SubItems.Add("");

            // Create leathers & cloths
            item = listView1.Items.Add("Leather");
            item.Group = listView1.Groups[nGroupLeatherTextiles];
            item.Tag = MaterialMeta.Leather;
            item.SubItems.Add("");
            item.SubItems.Add("10% Ranged Damage");
            item.SubItems.Add("10% Attack Speed");
            item.SubItems.Add("");
            item.SubItems.Add("10% Damage Avoidance");

            item = listView1.Items.Add("Soft Leather"); // aka Supple leather
            item.Group = listView1.Groups[nGroupLeatherTextiles];
            item.Tag = MaterialMeta.SuppleLeather;
            item.SubItems.Add("");
            item.SubItems.Add("20% Range Bonus");
            item.SubItems.Add("20% Damage Resistance");
            item.SubItems.Add("");
            item.SubItems.Add("20% Damage Resistance");

            item = listView1.Items.Add("Hard Leather"); // aka Hardened leather
            item.Group = listView1.Groups[nGroupLeatherTextiles];
            item.Tag = MaterialMeta.HardenedLeather;
            item.SubItems.Add("");
            item.SubItems.Add("20% Ranged Damage");
            item.SubItems.Add("20% Damage Avoidance");
            item.SubItems.Add("");
            item.SubItems.Add("20% Damage Avoidance");

            item = listView1.Items.Add("Cotton");
            item.Group = listView1.Groups[nGroupLeatherTextiles];
            item.Tag = MaterialMeta.Cotton;
            item.SubItems.Add("");
            item.SubItems.Add("10% Magic Damage");
            item.SubItems.Add("1 Intelligence");
            item.SubItems.Add("");
            item.SubItems.Add("10% Magic Damage");

            item = listView1.Items.Add("Fustian");
            item.Group = listView1.Groups[nGroupLeatherTextiles];
            item.Tag = MaterialMeta.Fustian;
            item.SubItems.Add("");
            item.SubItems.Add("20% Magic Damage");
            item.SubItems.Add("2 Intelligence");
            item.SubItems.Add("");
            item.SubItems.Add("2 Intelligence");

            item = listView1.Items.Add("Carapacian");
            item.Group = listView1.Groups[nGroupLeatherTextiles];
            item.Tag = MaterialMeta.Carapacian;
            item.SubItems.Add("");
            item.SubItems.Add("20% Attack Speed");
            item.SubItems.Add("20% Magic Damage");
            item.SubItems.Add("");
            item.SubItems.Add("20% Magic Damage");

            CreateMetaText();

            richTextBox1.Rtf = Properties.Resources.GeneralInformation;
        }

        private void CreateMetaText()
        {
            MaterialMetaText = new Dictionary<MaterialMeta, string>();

            MaterialMetaText.Add(MaterialMeta.Iron,             Properties.Resources.Iron);
            MaterialMetaText.Add(MaterialMeta.MeteoricIron,     Properties.Resources.MeteoricIron);
            MaterialMetaText.Add(MaterialMeta.Copper,           Properties.Resources.Copper);
            MaterialMetaText.Add(MaterialMeta.Bronze,           Properties.Resources.Bronze);
            MaterialMetaText.Add(MaterialMeta.WhiteIron,        Properties.Resources.WhiteIron);
            MaterialMetaText.Add(MaterialMeta.Constantan,       Properties.Resources.Constantan);

            MaterialMetaText.Add(MaterialMeta.Leather,          Properties.Resources.Leather);
            MaterialMetaText.Add(MaterialMeta.SuppleLeather,    Properties.Resources.SuppleLeather);
            MaterialMetaText.Add(MaterialMeta.HardenedLeather,  Properties.Resources.HardenedLeather);
            MaterialMetaText.Add(MaterialMeta.Cotton,           Properties.Resources.Cotton);
            MaterialMetaText.Add(MaterialMeta.Fustian,          Properties.Resources.Fustian);
            MaterialMetaText.Add(MaterialMeta.Carapacian,       Properties.Resources.Carapacian);

            MaterialMetaText.Add(MaterialMeta.Pine,             Properties.Resources.Pine);
            MaterialMetaText.Add(MaterialMeta.GreyPine,         Properties.Resources.GreyPine);
            MaterialMetaText.Add(MaterialMeta.CrimsonPine,      Properties.Resources.CrimsonPine);
            MaterialMetaText.Add(MaterialMeta.Maple,            Properties.Resources.Maple);
            MaterialMetaText.Add(MaterialMeta.HardMaple,        Properties.Resources.HardMaple);
            MaterialMetaText.Add(MaterialMeta.RockMaple,        Properties.Resources.RockMaple);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            listView1.Columns[1].Width = checkBox1.Checked ? columnWidth : 0;
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            listView1.Columns[2].Width = checkBox2.Checked ? columnWidth : 0;
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            listView1.Columns[3].Width = checkBox3.Checked ? columnWidth : 0;
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            listView1.Columns[4].Width = checkBox4.Checked ? columnWidth : 0;
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            listView1.Columns[5].Width = checkBox5.Checked ? columnWidth : 0;
        }

        private void listView1_ColumnWidthChanging(object sender, ColumnWidthChangingEventArgs e)
        {
            checkBox1.Checked = listView1.Columns[1].Width > 0;
            checkBox2.Checked = listView1.Columns[2].Width > 0;
            checkBox3.Checked = listView1.Columns[3].Width > 0;
            checkBox4.Checked = listView1.Columns[4].Width > 0;
            checkBox5.Checked = listView1.Columns[5].Width > 0;
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string rtf = null;

            if(listView1.SelectedIndices.Count == 0)
            {
                rtf = Properties.Resources.GeneralInformation;
            }

            else if(listView1.SelectedItems[0].Tag != null)
            {
                MaterialMeta meta = (MaterialMeta)listView1.SelectedItems[0].Tag;

                if (!MaterialMetaText.ContainsKey(meta))
                    return;

                rtf = MaterialMetaText[meta];
            }

            richTextBox1.Rtf = rtf;
        }

        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {
            splitContainer1.Panel2Collapsed = !checkBox6.Checked;
        }
    }
}
