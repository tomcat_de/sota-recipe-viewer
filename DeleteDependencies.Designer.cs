﻿namespace SotaDB_CSV_Edition
{
    partial class DeleteDependencies
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.directoryEntry1 = new System.DirectoryServices.DirectoryEntry();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonOK = new System.Windows.Forms.Button();
            this.listViewDependencies = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonRemoveReference = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonReplaceWith = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Dependencies";
            // 
            // buttonOK
            // 
            this.buttonOK.Enabled = false;
            this.buttonOK.Location = new System.Drawing.Point(430, 402);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(84, 23);
            this.buttonOK.TabIndex = 4;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // listViewDependencies
            // 
            this.listViewDependencies.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.listViewDependencies.FullRowSelect = true;
            this.listViewDependencies.HideSelection = false;
            this.listViewDependencies.Location = new System.Drawing.Point(12, 29);
            this.listViewDependencies.Name = "listViewDependencies";
            this.listViewDependencies.Size = new System.Drawing.Size(502, 339);
            this.listViewDependencies.TabIndex = 5;
            this.listViewDependencies.UseCompatibleStateImageBehavior = false;
            this.listViewDependencies.View = System.Windows.Forms.View.Details;
            this.listViewDependencies.SelectedIndexChanged += new System.EventHandler(this.listViewDependencies_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Item to delete";
            this.columnHeader1.Width = 98;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Affected Item";
            this.columnHeader2.Width = 112;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Action";
            this.columnHeader3.Width = 141;
            // 
            // buttonRemoveReference
            // 
            this.buttonRemoveReference.Enabled = false;
            this.buttonRemoveReference.Location = new System.Drawing.Point(15, 22);
            this.buttonRemoveReference.Name = "buttonRemoveReference";
            this.buttonRemoveReference.Size = new System.Drawing.Size(104, 25);
            this.buttonRemoveReference.TabIndex = 6;
            this.buttonRemoveReference.Text = "Remove";
            this.buttonRemoveReference.UseVisualStyleBackColor = true;
            this.buttonRemoveReference.Click += new System.EventHandler(this.buttonRemoveReference_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonReplaceWith);
            this.groupBox1.Controls.Add(this.buttonRemoveReference);
            this.groupBox1.Location = new System.Drawing.Point(12, 381);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(243, 62);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Action";
            // 
            // buttonReplaceWith
            // 
            this.buttonReplaceWith.Enabled = false;
            this.buttonReplaceWith.Location = new System.Drawing.Point(125, 21);
            this.buttonReplaceWith.Name = "buttonReplaceWith";
            this.buttonReplaceWith.Size = new System.Drawing.Size(104, 25);
            this.buttonReplaceWith.TabIndex = 6;
            this.buttonReplaceWith.Text = "Replace...";
            this.buttonReplaceWith.UseVisualStyleBackColor = true;
            this.buttonReplaceWith.Click += new System.EventHandler(this.buttonReplaceWith_Click);
            // 
            // DeleteDependencies
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(526, 455);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.listViewDependencies);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "DeleteDependencies";
            this.Text = "Item Dependencies";
            this.Load += new System.EventHandler(this.DeleteDependencies_Load);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.DirectoryServices.DirectoryEntry directoryEntry1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.ListView listViewDependencies;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Button buttonRemoveReference;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonReplaceWith;
    }
}