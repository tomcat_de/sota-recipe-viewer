﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SotaDB_CSV_Edition
{
    public class Recipe
    {
        public string name { get; set; }
        public string[] input { get; set; }
        public Skill skill { get; set; }

        public void SetInput(string s)
        {
            input = s.Split(new char[] { ',' });

            for (int i = 0; i < input.Length; ++i)
            {
                input[i] = input[i].Trim();
            }
        }

        public void SetSkill(string s)
        {
            string comp = s.ToUpperInvariant();

            if (comp.CompareTo("ALCHEMY") == 0)
                skill = Skill.Alchemy;
            else if (comp.CompareTo("BLACKSMITHING") == 0)
                skill = Skill.Blacksmithing;
            else if (comp.CompareTo("BUTCHERY") == 0)
                skill = Skill.Butchery;
            else if (comp.CompareTo("CARPENTRY") == 0)
                skill = Skill.Carpentry;
            else if (comp.CompareTo("COOKING") == 0)
                skill = Skill.Cooking;
            else if (comp.CompareTo("GATHERING") == 0)
                skill = Skill.Gathering;
            else if (comp.CompareTo("MILLING") == 0)
                skill = Skill.Milling;
            else if (comp.CompareTo("SMELTING") == 0)
                skill = Skill.Smelting;
            else if (comp.CompareTo("TAILORING") == 0)
                skill = Skill.Tailoring;
            else if (comp.CompareTo("TANNING") == 0)
                skill = Skill.Tanning;
            else if (comp.CompareTo("TEXTILES") == 0)
                skill = Skill.Textiles;
            else if (comp.CompareTo("TOOL") == 0)
                skill = Skill.Tool;
            else
                throw new ArgumentException();
        }

        public enum Skill
        {
            All,
            Alchemy,
            Blacksmithing,
            Butchery,
            Carpentry,
            Cooking,
            Gathering,
            Milling,
            Smelting,
            Tailoring,
            Tanning,
            Textiles,
            Tool
        };

        public enum Category
        {
            Gathering,
            Production,
            Refining,
            Tool
        };
    };
}
