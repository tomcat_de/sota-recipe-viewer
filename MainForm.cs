﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;

using iTextSharp.text;
using iTextSharp.text.pdf;

namespace SotaDB_CSV_Edition
{
    public partial class MainForm : Form
    {
        private Materials materials;

        public MainForm()
        {
            InitializeComponent();
        }

        readonly List<Recipe> Recipes = new List<Recipe>();
        bool Changed;

        private bool LoadCSV(string path)
        {
            if (!File.Exists(path))
                return false;

            char[] splitter = { ',' };

            using (var stream = File.OpenText(path))
            {
                Recipes.Clear();

                var firstLine = stream.ReadLine();

                var categories = firstLine.Split(splitter);

                if (categories.Length != 4)
                {
                    return false;
                }

                while (true)
                {
                    var line = stream.ReadLine();

                    if (line == null)
                        break;

                    Recipe recipe = new Recipe();

                    // Expect 4 categories
                    int startIndex = 0;

                    for (int i = 0; i < 4; ++i)
                    {
                        // Ingredients -> Special treatment because of quotation marks and extra commas
                        if (i == 2)
                        {
                            if (line[startIndex] == ',')
                            {
                                startIndex += 1;
                            }
                            else
                            {
                                string itemList = null;

                                if (line[startIndex] == '\"')
                                {
                                    int closingQuotation = line.IndexOf('\"', startIndex + 1);

                                    if (closingQuotation == -1)
                                        continue;

                                    itemList = line.Substring(startIndex + 1, closingQuotation - startIndex - 1);

                                    startIndex = closingQuotation + 2;
                                }
                                else
                                {
                                    int nextComma = line.IndexOf(',', startIndex + 1);
                                    itemList = line.Substring(startIndex, nextComma - startIndex);
                                    startIndex = nextComma + 1;
                                }

                                recipe.SetInput(itemList);
                            }
                        }

                        // Everything else
                        else
                        {
                            int nextComma = (i < 3 ? line.IndexOf(',', startIndex + 1) : line.Length);

                            if (nextComma == -1)
                                continue;

                            string s = line.Substring(startIndex, nextComma - startIndex);

                            if (i == 0)
                                recipe.name = s;
                            else if (i == 1)
                                recipe.SetSkill(s);

                            startIndex = nextComma + 1;
                        }
                    }

                    Recipes.Add(recipe);
                }
            }

            return true;
        }

        private enum SortBy
        {
            Name,
            Skill
        }

        private void SortList(SortBy sortBy = SortBy.Name, bool descending = false)
        {
            switch (sortBy)
            {
                case SortBy.Name:

                    if (descending)
                        Recipes.Sort((a, b) => b.name.CompareTo(a.name));
                    else
                        Recipes.Sort((a, b) => a.name.CompareTo(b.name));

                    break;

                case SortBy.Skill:

                    if (descending)
                        Recipes.Sort((a, b) => b.skill.ToString().CompareTo(a.skill.ToString()));
                    else
                        Recipes.Sort((a, b) => a.skill.ToString().CompareTo(b.skill.ToString()));

                    break;
            }
        }

        private void FillListView()
        {
            FillListView((Recipe.Skill)comboBoxSkill.SelectedItem, textBoxFilter.Text);
        }

        private void FillListView(Recipe.Skill filterSkill, string filter)
        {
            listView1.Items.Clear();
            deleteToolStripMenuItem.Enabled = false;

            if (filter?.Length == 0)
                filter = null;

            string ucFilter = filter?.ToUpperInvariant();

            int n = 0;

            foreach (var recipe in Recipes)
            {
                // Apply Skill filter
                if (recipe.skill != filterSkill && filterSkill != Recipe.Skill.All)
                    continue;

                // Apply text filter
                if(ucFilter != null)
                {
                    if(!recipe.name.ToUpperInvariant().Contains(ucFilter))
                    {
                        continue;
                    }
                }

                var item = listView1.Items.Add(recipe.name);

                item.Tag = recipe;

                if (recipe.input?.Length > 0)
                {
                    StringBuilder sb = new StringBuilder();

                    for (int i = 0; i < recipe.input.Length; ++i)
                    {
                        sb.Append(recipe.input[i]);

                        if (i < recipe.input.Length - 1)
                            sb.Append(", ");
                    }

                    item.SubItems.Add(sb.ToString());
                }
                else
                    item.SubItems.Add("");

                item.SubItems.Add(recipe.skill.ToString());

                n += 1;
            }

            foreach(ColumnHeader column in listView1.Columns)
                column.AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent);

            statusStrip1.Items[0].Text = $"Showing {n} of {Recipes.Count} recipes";
        }

        private void PopulateSkillList()
        {
            comboBoxSkill.Items.Add(Recipe.Skill.All);
            comboBoxSkill.Items.Add(Recipe.Skill.Alchemy);
            comboBoxSkill.Items.Add(Recipe.Skill.Blacksmithing);
            comboBoxSkill.Items.Add(Recipe.Skill.Butchery);
            comboBoxSkill.Items.Add(Recipe.Skill.Carpentry);
            comboBoxSkill.Items.Add(Recipe.Skill.Cooking);
            comboBoxSkill.Items.Add(Recipe.Skill.Gathering);
            comboBoxSkill.Items.Add(Recipe.Skill.Milling);
            comboBoxSkill.Items.Add(Recipe.Skill.Smelting);
            comboBoxSkill.Items.Add(Recipe.Skill.Tailoring);
            comboBoxSkill.Items.Add(Recipe.Skill.Tanning);
            comboBoxSkill.Items.Add(Recipe.Skill.Textiles);
            comboBoxSkill.Items.Add(Recipe.Skill.Tool);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            PopulateSkillList();

            comboBoxSkill.SelectedIndex = 0;

            if (LoadCSV("recipes.csv"))
            {
                SortList(SortBy.Name);

                FillListView();
            }
        }

        private void comboBoxSkill_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillListView();
        }

        private void textBoxFilter_TextChanged(object sender, EventArgs e)
        {
            Recipe.Skill skill = Recipe.Skill.All;

            if (comboBoxSkill.SelectedItem != null)
                skill = (Recipe.Skill)comboBoxSkill.SelectedItem;

            FillListView();
        }

        private void buttonClearFilter_Click(object sender, EventArgs e)
        {
            textBoxFilter.Text = "";
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void exportCSVToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();

            dlg.Filter = "Comma Separated Values (*.csv)|*.csv|Text file (*.txt)|*.txt|All files (*.*)|*.*";

            var result = dlg.ShowDialog();

            if (result == DialogResult.OK)
            {
                ExportCSV(dlg.FileName);

                Changed = false;
            }
        }

        private void ExportCSV(string FileName)
        {
            using (StreamWriter file = new StreamWriter(FileName))
            {
                file.Write("RecipeName,Skill,Ingredients,Category\r\n");

                foreach (Recipe recipe in Recipes)
                {
                    StringBuilder sb = new StringBuilder();

                    // Name,Skill,Ingredients,Category

                    sb.Append(recipe.name);
                    sb.Append(',');

                    sb.Append(recipe.skill.ToString());
                    sb.Append(',');

                    if (recipe.input != null)
                    {
                        if (recipe.input.Length == 1)
                        {
                            sb.Append(recipe.input[0]);
                        }
                        else if (recipe.input.Length > 1)
                        {
                            sb.Append('\"');

                            for (int i = 0; i < recipe.input.Length; ++i)
                            {
                                sb.Append(recipe.input[i]);

                                if (i < (recipe.input.Length - 1))
                                    sb.Append(',');
                            }

                            sb.Append('\"');
                        }
                    }

                    sb.Append(',');

                    //sb.Append(recipe.category.ToString());
                    sb.Append("\r\n");

                    file.Write(sb);
                }
            }
        }

        private void addRecipeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (AddRecipeForm addRecipe = new AddRecipeForm(Recipes))
            {
                var result = addRecipe.ShowDialog(this);

                if (result == DialogResult.OK)
                {
                    Recipes.Add(addRecipe.recipe);

                    SortList(SortBy.Name);

                    FillListView();

                    Changed = true;
                }
            }
        }

        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count != 1)
                return;

            Recipe recipe = listView1.SelectedItems[0].Tag as Recipe;

            if (recipe == null)
                return;

            using (AddRecipeForm form = new AddRecipeForm(Recipes))
            {
                form.recipe = recipe;

                var result = form.ShowDialog();

                if(result == DialogResult.OK)
                {
                    Changed = true;

                    SortList(SortBy.Name);

                    FillListView();
                }
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Changed)
            {
                var result = MessageBox.Show("Do you want to save your changes?", "SotA Recipe Viewer", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                if (result == DialogResult.Cancel)
                {
                    e.Cancel = true;
                    return;
                }
                else if(result == DialogResult.Yes)
                {
                    using (SaveFileDialog dlg = new SaveFileDialog())
                    {
                        dlg.Filter = "Comma Separated Values (*.csv)|*.csv|Text file (*.txt)|*.txt|All files (*.*)|*.*";

                        var dlgResult = dlg.ShowDialog();

                        if (dlgResult == DialogResult.Cancel)
                        {
                            e.Cancel = true;
                            return;
                        }

                        ExportCSV(dlg.FileName);
                    }
                }
            }
        }

        private void importCSVToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Filter = "Comma Separated Values (*.csv)|*.csv|Text file (*.txt)|*.txt|All files (*.*)|*.*";

                var result = dlg.ShowDialog();

                if(result == DialogResult.OK)
                {
                    LoadCSV(dlg.FileName);

                    SortList();

                    FillListView();
                }
            }
        }

        private void materialsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (materials != null && materials.Visible)
            {
                materials.BringToFront();
                return;
            }

            materials = new Materials(Recipes);
            materials.Show();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var count = listView1.SelectedIndices.Count;

            if (count < 1)
                return;

            // Let user confirm deletion and show dependencies
            var candidatesForDeletion = new List<Recipe>();

            foreach (ListViewItem item in listView1.SelectedItems)
            {
                candidatesForDeletion.Add(item.Tag as Recipe);
            }

            Dictionary<Recipe, List<DeleteDependencies.DependencyInfo>> dependencies;

            DeleteDependencies.FindDependencies(candidatesForDeletion, Recipes, out dependencies);

            if (dependencies.Count > 0)
            {
                var dlg = new DeleteDependencies(candidatesForDeletion, Recipes, dependencies);

                var result = dlg.ShowDialog();

                if (result != DialogResult.OK)
                    return;
            }

            foreach (var item in dependencies)
            {
                foreach (var dep in item.Value)
                {
                    if (dep.action == DeleteDependencies.DependencyInfo.Action.RemoveIngredient)
                    {
                        string name = item.Key.name.Trim().ToUpperInvariant();

                        for (int i = 0; i < dep.recipe.input.Length; ++i)
                        {
                            string s = dep.recipe.input[i];

                            int idx = s.LastIndexOf('(');

                            string cmp = (idx > -1 ? s.Substring(0, idx) : s).Trim().ToUpperInvariant();

                            if (cmp.CompareTo(name) == 0)
                            {
                                // Create new item list
                                if (dep.recipe.input.Length > 1)
                                {
                                    string[] newList = new string[dep.recipe.input.Length - 1];

                                    int idxWrite = 0;

                                    for (int j = 0; j < dep.recipe.input.Length; ++j)
                                    {
                                        if (j == i)
                                            continue;

                                        newList[idxWrite++] = dep.recipe.input[j];
                                    }

                                    dep.recipe.input = newList;
                                }

                                // Removed last item -> list now empty
                                else
                                {
                                    dep.recipe.input = null;
                                }

                                break;
                            }
                        }
                    }

                    else if (dep.action == DeleteDependencies.DependencyInfo.Action.Replace)
                    {
                        string name = item.Key.name.Trim().ToUpperInvariant();

                        for (int i = 0; i < dep.recipe.input.Length; ++i)
                        {
                            string s = dep.recipe.input[i];

                            int idx = s.LastIndexOf('(');

                            string cmp = (idx > -1 ? s.Substring(0, idx) : s).Trim().ToUpperInvariant();

                            if (cmp.CompareTo(name) == 0)
                            {
                                dep.recipe.input[i] = dep.recipeReplacement;
                                break;
                            }
                        }
                    }
                }
            }

            foreach (Recipe rip in candidatesForDeletion)
            {
                Recipes.Remove(rip);
            }

            Changed = true;

            FillListView();
        }

        private void newRecipeBookToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(Changed)
            {
                var response = MessageBox.Show("Do you want to save your changes first?", "SotA Recipe Viewer", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                if (response == DialogResult.Cancel)
                    return;

                else if (response == DialogResult.Yes)
                {
                    SaveFileDialog dlg = new SaveFileDialog();

                    var result = dlg.ShowDialog();

                    if (result != DialogResult.OK)
                        return;

                    ExportCSV(dlg.FileName);

                    Changed = false;
                }
            }


            // Start new list
            Recipes.Clear();

            FillListView();
        }

        private void listView1_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            deleteToolStripMenuItem.Enabled = listView1.SelectedItems.Count > 0;
            generatePDFToolStripMenuItem.Enabled = listView1.SelectedItems.Count > 0;
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var about = new AboutForm())
            {
                about.ShowDialog(this);
            }
        }

        private void generatePDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count < 1)
                return;

            string fileName;

            using (var dlg = new SaveFileDialog() { Filter = "Portable Document Format (*.pdf)|*.pdf" })
            {
                var result = dlg.ShowDialog();

                if (result != DialogResult.OK)
                    return;

                fileName = dlg.FileName;
            }

            var pdfDoc = new Document(new Rectangle(PageSize.A4));

            var stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None);
            var writer = PdfWriter.GetInstance(pdfDoc, stream);

            pdfDoc.Open();

            foreach (ListViewItem item in listView1.SelectedItems)
            {
                var recipe = item.Tag as Recipe;

                var p = new Paragraph(recipe.name);

                pdfDoc.Add(p);
            }

            pdfDoc.Close();

            pdfDoc.Dispose();
            stream.Dispose();

            // Open PDF in default program
            System.Diagnostics.Process.Start(fileName);
        }
    }
}