﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace SotaDB_CSV_Edition
{
    public partial class DeleteDependencies : Form
    {
        readonly IEnumerable<Recipe> recipes;
        readonly IEnumerable<Recipe> recipesToDelete;

        public class DependencyInfo
        {
            public DependencyInfo(Recipe recipe, Action action = Action.NotSet)
            {
                this.recipe             = recipe;
                this.action             = action;
                this.recipeReplacement  = null;
            }

            public Recipe recipe { get; set; }

            public enum Action
            {
                NotSet,             // Nothing selected
                DeleteDependant,    // Delete entire dependent
                RemoveIngredient,   // Only remove mentioning in ingredient list
                Replace,            // Replace with other ingredient
            };

            public Action action { get; set; }

            public string recipeReplacement;
        }

        public readonly Dictionary<Recipe, List<DependencyInfo>> dependencies;

        public DeleteDependencies(IEnumerable<Recipe> recipesToDelete, IEnumerable<Recipe> recipes, Dictionary<Recipe, List<DependencyInfo>> dependencies)
        {
            this.recipes = recipes;
            this.recipesToDelete = recipesToDelete;
            this.dependencies = dependencies;

            InitializeComponent();

            //FindDependencies(recipesToDelete, recipes, out dependencies);
        }

        public static void FindDependencies (
                                                IEnumerable<Recipe>                             recipesToDelete,
                                                IEnumerable<Recipe>                             recipes,
                                                out Dictionary<Recipe, List<DependencyInfo>>    dependencies
                                            )
        {
            dependencies = new Dictionary<Recipe, List<DependencyInfo>>();

            foreach(Recipe recipe in recipesToDelete)
            {
                var depList = new List<DependencyInfo>();

                var name = recipe.name.ToUpperInvariant().Trim();

                foreach(Recipe recipe2 in recipes)
                {
                    if (recipe == recipe2 || recipe2.input == null)
                        continue;

                    foreach(string strInput in recipe2.input)
                    {
                        // Get item's name without quantity information
                        var idxQuantity = strInput.LastIndexOf('(');

                        var name2 = (idxQuantity > -1 ? strInput.Substring(0, idxQuantity - 1) : strInput).Trim().ToUpperInvariant();

                        // Compare
                        if(name2.CompareTo(name) == 0)
                        {
                            depList.Add(new DependencyInfo(recipe2));
                        }
                    }
                }

                if(depList.Count > 0)
                {
                    dependencies[recipe] = depList;
                }
            }
        }

        private void DeleteDependencies_Load(object sender, EventArgs e)
        {
            foreach (var it in dependencies)
            {
                var                  recipe   = it.Key;
                var    depList  = it.Value;

                foreach(DependencyInfo dep in depList)
                {
                    var item = new ListViewItem(recipe.name);
                    item.SubItems.Add(dep.recipe.name);
                    item.SubItems.Add("Not set");
                    item.Tag = dep;
                    listViewDependencies.Items.Add(item);
                }
            }
        }

        private void listViewDependencies_SelectedIndexChanged(object sender, EventArgs e)
        {
            buttonReplaceWith.Enabled =
            buttonRemoveReference.Enabled = listViewDependencies.SelectedItems.Count > 0;
        }

        private void buttonRemoveReference_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem sel in listViewDependencies.SelectedItems)
            {
                var info = (DependencyInfo) sel.Tag;

                info.action = DependencyInfo.Action.RemoveIngredient;

                sel.SubItems[2].Text = "Remove ingredient";
            }

            UpdateOKButton();
        }

        private void buttonReplaceWith_Click(object sender, EventArgs e)
        {
            // Present dialog to choose valid replacements
            var validReplacements = new List<Recipe>();

            foreach(Recipe r in recipes)
            {
                var valid = true;

                //
                // Invalid replacement recipes are:
                //
                //   - Ones that are to be deleted (in recipesToDelete)
                //   - Ones that are marked for deletion (Action.Delete)
                //

                foreach(Recipe del in recipesToDelete)
                {
                    if(r == del)
                    {
                        valid = false;
                        break;
                    }
                }

                if (!valid)
                    continue;

                foreach(var dep in dependencies)
                {
                    foreach(DependencyInfo info in dep.Value)
                    {
                        if (info.recipe == r && info.action == DependencyInfo.Action.DeleteDependant)
                        {
                            valid = false;
                            break;
                        }
                    }

                    if (!valid)
                        break;
                }

                if(valid)
                {
                    validReplacements.Add(r);
                }
            }


            using (var frm = new AddRecipeItemForm(validReplacements))
            {
                frm.Text = "Choose replacement";
                var result = frm.ShowDialog();

                if (result != DialogResult.OK)
                    return;

                // Update list
                foreach (ListViewItem sel in listViewDependencies.SelectedItems)
                {
                    var info = (DependencyInfo)sel.Tag;

                    info.action = DependencyInfo.Action.Replace;
                    info.recipeReplacement = frm.Item;

                    sel.SubItems[2].Text = $"Replace with {frm.Item}";
                }

                UpdateOKButton();
            }
        }

        private void UpdateOKButton()
        {
            buttonOK.Enabled = false;

            foreach(var item in dependencies)
            {
                foreach(var subItem in item.Value)
                {
                    if (subItem.action == DependencyInfo.Action.NotSet)
                        return;
                }
            }

            buttonOK.Enabled = true;
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
