﻿*********************
**  High priority  **
*********************

	- Dependency logic: Every time the user changes dependency resolving, check all actions again
					    (Can't replace an ingredient with an ingredient that has now been marked for deletion)

						Only a recipe in the dependency-resolver-list when there are no aliases for the
						same materials.

	- Renaming Recipes:
	
		1) If no other recipes with the same (old) name exist: Update all references
		2) If recipes with the same (old) name exist: Ask whether to:
		
			a) Rename all the others as well and update all references  -OR-
			b) Update references to new name -OR-
			c) Keep The old name in references

	- Implement sorting by clicking on the listview's header


	
***********************
**  Medium priority  **
***********************

	- Allow item groups in recipes. That is, a palceholder that can stand for a variety of ingredients to yield different
	  results (for example, the Group "{metal ingot}" could stand for any of: Iron Ingot, Copper Ingot, Bronze Ingot, ...)

	- Store in SQLite instead of CSV

	- Implement filter in Materials dialog
	
	- Let user edit an ingredient's quantity

	- Materials window: Quick navigation to material's recipe



********************
**  Low priority  **
********************

	- Add column for recipe base XP

	- Allow meta information (rich text?) for each recipe to annotate such information as where to find resource nodes
	





***************
**  Unknown  **
***************

	- Scan local SotA files for recipes??

	- Sync from remote DB

	- 