# README #

Hello and thank you for your interest in SotA Recipe Viewer!

### What it is ###

* SotA Recipe Viewer is a simple tool to help players of Lord British's [Shroud of the Avatar](http://www.shroudoftheavatar.com) with crafting
* It requires Microsoft Windows with .NET Framework 4 or higher (or Mono equivalent)

### Features ###

* Manage recipes in as many books as you like
* Filtering and search of recipes
* Easy sharing of recipe books in CSV format
* Overview of crafting materials, their properties and most common recipes

### Screenshots ###

![RecipeViewer2.png](https://bitbucket.org/repo/4rMz9B/images/1366176307-RecipeViewer2.png)

![RecipeViewer1.png](https://bitbucket.org/repo/4rMz9B/images/3059499395-RecipeViewer1.png)

### Contribute! ###

If you want to contribute to the tool, you're more than welcome to do so! You need:

* Visual Studio 2015 Community Edition or higher, free download [here](https://www.visualstudio.com/downloads/download-visual-studio-vs)

### Credits ###

* **Shroud of the Avatar** and the Shroud of the Avatar Logo is property of Portalarium, Inc.

### Contact ###

* If you have any questions, ideas, criticism or whatever, please feel free to contact me: friedrich.huber (at) gmx.de